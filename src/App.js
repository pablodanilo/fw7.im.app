import React from 'react';
import './App.css';
import LeftContainer from './components/LeftContainer'
import RightContainer from './components/RightContainer'

function App() {
  return (
    <>
      <LeftContainer />
      <RightContainer /> 
    </>
  )
}

export default App;
