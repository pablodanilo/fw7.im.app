import React from 'react'
import { Container, Title, Subtitle} from './styles'

export default () => {
  return  <Container>
            <Title>GERENCIAMENTO ONLINE DE DAEMONS RODANDO NOS CLIENTES</Title>
            <Subtitle>Com o sistema de gerenciamento de deamons, você pode executar uma série de tarefas sem a necessidade de conectar remotamente nos clientes.</Subtitle>
          </Container>
}