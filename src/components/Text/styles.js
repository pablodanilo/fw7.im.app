import styled from 'styled-components'

export const Container = styled.div`
  justify-content: center;
  align-items: center;
  margin: 70px;
  color: white;
`

export const Title = styled.h1`
  @media (max-width: 1366px) {
    font-size: 2.3em;
  }

  text-align: left;
  font-size: 3.4em;
  font-weight: 800;
`

export const Subtitle = styled.span`
  @media (max-width: 1366px) {
    font-size: 1.4em;
  }

  text-align: left;
  font-size: 2em;
  font-weight: 300;
`