import React from 'react'

import { StyledButton } from './styles'

export const Button = ({ children }) => <StyledButton> {children} </StyledButton>
