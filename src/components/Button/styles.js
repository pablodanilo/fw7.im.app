import styled from 'styled-components'

export const StyledButton = styled.a`
  background-color: #379BCC;
  display: flex;
  align-items: center;
  justify-content: center;
  height: 25px;
  width: 100%;
  color: white;
  font-size: 16px;
  font-weight: 700;
  padding: 10px 0px;
  border: 1px solid #379BCC;
  border-radius: 5px;
  margin-top: 40px;
  font-family: montserrat;  
`