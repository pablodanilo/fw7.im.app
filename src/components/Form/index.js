import React from 'react'
import Input from '../Input'
import { Button } from '../Button'
import Logo from '../Logo'

import { Container } from './styles'

export default () => (
  <Container>
    <Logo />

    <Input
      label='E-mail'
      type="email"
      placeholder="seuemail@fw7.com.br"
      id="input_email"
    />
    <Input
      label='Password'
      type="password"
      placeholder="Qual sua senha?"
      id="input_senha"
    />

    <Button>ENTRAR</Button>
  </Container>
)
