import styled from 'styled-components'

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
`

export const PrimaryName = styled.span`
  color: #305A8B;
  font-family: Poppins;
  font-weight: 700;
  font-size: 1.8em;
`

export const SecondaryName = styled.span`
  color: #305A8B; 
  font-family: Poppins;
  font-weight: 300;
  font-size: 15px;
  margin-top: -6px;
`