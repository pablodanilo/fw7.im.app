import styled from 'styled-components'

export const Container = styled.div`
  background-color: #379BCC;
  display: flex;
  width: 50%;
  height: 100vh;
  justify-content: center;
  align-items: center;
`