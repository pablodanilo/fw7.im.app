import React from 'react'
import Text from '../Text'
import { Container } from './styles'

export default () => (
  <Container> <Text /> </Container>
)
