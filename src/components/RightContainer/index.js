import React from 'react'
import Form from '../Form'
import { Container } from './styles'

export default () =>  (
  <Container>
    <Form />
  </Container>
)
