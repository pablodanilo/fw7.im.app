import React from 'react'
import Image from '../Image'
import Name from '../Name'
import { Container } from './styles'

export default () =>  (
  <Container>
    <Image />
    <Name />
  </Container>
)