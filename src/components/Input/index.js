import React from 'react'
import { Container, Label, Input } from './styles'

export default props => (
  <Container>
    <Label>{props.label}</Label>
    <Input {...props} />
  </Container>
)