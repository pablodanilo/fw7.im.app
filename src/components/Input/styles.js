import styled from 'styled-components'

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
  margin: 0px;
`

export const Label = styled.label`
  color: #777;
`

export const Input = styled.input`
  margin: 5px 0px 15px 0px;
  height: 25px;
  border-radius: 5px;
  border: 1px solid #9b9b9b;
  font-size: 14px;
  font-family: montserrat;
  outline: none;
  padding: 10px;
  color: #777;
  &::placeholder {
    opacity: 0.6;
  }
`