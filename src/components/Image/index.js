import React from 'react'
import logo from '../../assets/images/logo-fw7.svg'
import { Image } from './styles'

export default () =>  (
  <Image src={logo} />
)