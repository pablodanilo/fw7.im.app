import styled from 'styled-components'

export const Image = styled.img`
  display: flex;
  justify-content: center;
  align-items: center;
  margin: 0px 30px 0px -30px;
`